//
//  ViewController.h
//  Location
//
//  Created by Click Labs 105 on 10/26/15.
//  Copyright (c) 2015 rohit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface ViewController : UIViewController<CLLocationManagerDelegate>


@end

